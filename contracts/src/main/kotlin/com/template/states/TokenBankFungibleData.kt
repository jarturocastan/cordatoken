package com.template.states

import com.r3.corda.lib.tokens.contracts.states.EvolvableTokenType
import net.corda.core.identity.Party
import net.corda.core.contracts.BelongsToContract
import com.template.TokenBankFungibleDataContract
import net.corda.core.contracts.ContractState
import net.corda.core.contracts.UniqueIdentifier
import net.corda.core.schemas.MappedSchema
import net.corda.core.schemas.PersistentState
import net.corda.core.schemas.QueryableState

@BelongsToContract(TokenBankFungibleDataContract::class)
class TokenBankFungibleData(
        val clabeOrigen: String,
        val clabeDestino: String,
        val from: String,
        val toParty: String,
        val transacctionId: String,
        val concepto: String,
        val timestampFinal: Long,
        val quantity: Double,
        val auditor: Party,
        val holder: Party,
        val fromParty: Party,
        val type : Int) :  QueryableState,ContractState
{
    override fun generateMappedObject(schema: MappedSchema): PersistentState  = TokenBankFungibleDataSchemaV1.PersistentTokenBankState(
            this.clabeOrigen,this.clabeDestino,this.concepto,this.from,this.toParty,this.transacctionId,this.type,this.timestampFinal,this.quantity
    )

    override fun supportedSchemas() = listOf(TokenBankFungibleDataSchemaV1)

    override val participants: List<Party> get() = listOf(auditor,holder,fromParty)

    companion object {
        val contractId = this::class.java.enclosingClass.canonicalName
    }
}