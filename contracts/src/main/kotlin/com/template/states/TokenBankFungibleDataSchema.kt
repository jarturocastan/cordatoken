package com.template.states

import net.corda.core.schemas.MappedSchema
import net.corda.core.schemas.PersistentState
import net.corda.core.serialization.CordaSerializable
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Table

object TokenBankFungibleDataSchema

@Suppress("MagicNumber") // SQL column length
@CordaSerializable
object TokenBankFungibleDataSchemaV1 : MappedSchema(schemaFamily = TokenBankFungibleDataSchema.javaClass, version = 1, mappedTypes = listOf(PersistentTokenBankState::class.java)) {

    override val migrationResource = "contract_token_bank_fungible_data.changelog-master";

    @Entity
    @Table(name = "contract_token_bank_fungible_data_states")
    class PersistentTokenBankState(
            @Column(name = "clabe_origen")
            var clabeOrigen: String,
            @Column(name = "clabe_destino")
            var clabeDestino: String,
            @Column(name = "concept")
            var concepto: String,
            @Column(name = "fromParty")
            var from: String,
            @Column(name = "toParty")
            var to: String,
            @Column(name = "tx_id")
            var tx_id: String,
            @Column(name = "type")
            var type: Int, /// 0 es issue 1 es move
            @Column(name = "tx_date")
            var date: Long,
            @Column(name = "quantity")
            var quantity: Double
    ) : PersistentState() {
        constructor() : this("","","","","","",0,0,0.0);
    }

}

