package com.template.states

import com.r3.corda.lib.tokens.contracts.states.EvolvableTokenType
import com.template.RealEstateEvolvableTokenTypeContract
import net.corda.core.contracts.BelongsToContract
import net.corda.core.contracts.UniqueIdentifier
import net.corda.core.identity.Party
import java.math.BigDecimal

@BelongsToContract(RealEstateEvolvableTokenTypeContract::class)
class RealEstateEvolvableTokenType(val valuation: BigDecimal,
                                   val maintainer: Party,
                                   val monitor: Party,
                                   override  val linearId: UniqueIdentifier,
                                   override val fractionDigits: Int,
                                   val symbol: String) : EvolvableTokenType() {

    companion object {
        val contractId = this::class.java.enclosingClass.canonicalName
    }

    override val maintainers: List<Party> get() = listOf(maintainer,monitor)
}
