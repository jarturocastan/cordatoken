package com.template.flows

import co.paralleluniverse.fibers.Suspendable
import com.r3.corda.lib.tokens.workflows.flows.rpc.CreateEvolvableTokens
import com.template.TokenBankFungibleDataContract
import com.template.states.TokenBankFungibleData
import net.corda.core.contracts.Command
import net.corda.core.contracts.TransactionState
import net.corda.core.contracts.UniqueIdentifier
import net.corda.core.contracts.requireThat
import net.corda.core.flows.*
import net.corda.core.identity.Party
import net.corda.core.transactions.SignedTransaction
import net.corda.core.transactions.TransactionBuilder
import java.math.BigDecimal


@InitiatingFlow
@StartableByRPC
class TokenBankFungibleDataFlow(val state: TokenBankFungibleData): FlowLogic<SignedTransaction>() {
    @Suspendable
    override fun call(): SignedTransaction {
     val notary = serviceHub.networkMapCache.notaryIdentities.first()

        val issueCommand = Command(TokenBankFungibleDataContract.Commands.Issue(), state.participants.map { it.owningKey })
        val builder = TransactionBuilder(notary = notary)
        builder.addOutputState(state, TokenBankFungibleDataContract.IOU_CONTRACT_ID)
        builder.addCommand(issueCommand)
        builder.verify(serviceHub)
        val ptx = serviceHub.signInitialTransaction(builder)
        val sessions = (state.participants - ourIdentity).map { initiateFlow(it) }.toSet()
        val stx = subFlow(CollectSignaturesFlow(ptx, sessions))
        return subFlow(FinalityFlow(stx, sessions))
    }
}


@InitiatedBy(TokenBankFungibleDataFlow::class)
class IOUIssueFlowResponder(val flowSession: FlowSession): FlowLogic<SignedTransaction>() {

    @Suspendable
    override fun call(): SignedTransaction {
        val signedTransactionFlow = object : SignTransactionFlow(flowSession) {
            override fun checkTransaction(stx: SignedTransaction) = requireThat {
                val output = stx.tx.outputs.single().data
                "This must be an TokenBankFungibleData transaction" using (output is TokenBankFungibleData)
            }
        }

        val txWeJustSignedId = subFlow(signedTransactionFlow)

        return subFlow(ReceiveFinalityFlow(otherSideSession = flowSession, expectedTxId = txWeJustSignedId.id))
    }
}