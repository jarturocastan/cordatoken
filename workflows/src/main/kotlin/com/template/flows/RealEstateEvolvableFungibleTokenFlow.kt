package com.template.flows

import co.paralleluniverse.fibers.Suspendable
import com.google.common.collect.ImmutableList
import com.r3.corda.lib.tokens.contracts.states.EvolvableTokenType
import com.r3.corda.lib.tokens.contracts.states.FungibleToken
import com.r3.corda.lib.tokens.contracts.types.IssuedTokenType
import com.r3.corda.lib.tokens.contracts.types.TokenType
import com.r3.corda.lib.tokens.contracts.utilities.heldBy
import com.r3.corda.lib.tokens.contracts.utilities.issuedBy
import com.r3.corda.lib.tokens.contracts.utilities.of
import com.r3.corda.lib.tokens.money.DigitalCurrency
import com.r3.corda.lib.tokens.money.FiatCurrency
import com.r3.corda.lib.tokens.workflows.flows.rpc.CreateEvolvableTokens
import com.r3.corda.lib.tokens.workflows.flows.rpc.*
import com.r3.corda.lib.tokens.workflows.types.PartyAndAmount
import com.r3.corda.lib.tokens.workflows.utilities.heldBy
import com.template.states.RealEstateEvolvableTokenType
import net.corda.core.contracts.Amount
import net.corda.core.contracts.LinearPointer
import net.corda.core.contracts.TransactionState
import net.corda.core.contracts.UniqueIdentifier
import net.corda.core.flows.FlowException
import net.corda.core.flows.FlowLogic
import net.corda.core.flows.StartableByRPC
import net.corda.core.identity.AbstractParty
import net.corda.core.identity.AnonymousParty
import net.corda.core.identity.CordaX500Name
import net.corda.core.identity.Party
import net.corda.core.node.services.Vault
import net.corda.core.node.services.queryBy
import net.corda.core.node.services.vault.QueryCriteria
import net.corda.core.transactions.SignedTransaction
import net.corda.core.utilities.ProgressTracker
import java.math.BigDecimal
import java.util.*
import javax.xml.ws.Holder


/**
     * Create Fungible Token for a house asset on ledger
     */
    @StartableByRPC
    class CreateEvolvableFungibleTokenFlow(private val valuation: BigDecimal,
                                           val monitor : Party,
                                            val symbol : String) : FlowLogic<SignedTransaction>() {

        @Suspendable
        @Throws(FlowException::class)
        override  fun call(): SignedTransaction {
            val notary =  serviceHub.networkMapCache.notaryIdentities.first()
            val evolvableTokenType = RealEstateEvolvableTokenType(valuation, ourIdentity, monitor, UniqueIdentifier(), 0, symbol)
            val transactionState = TransactionState(evolvableTokenType, notary = notary)
            return subFlow(CreateEvolvableTokens(transactionState))

        }
    }

    /**
     * Issue Fungible Token against an evolvable house asset on ledger
     */
    @StartableByRPC
    class IssueEvolvableFungibleTokenFlow(
            private val quantity: Long,
            private val holder: Party) : FlowLogic<SignedTransaction>() {

        override val progressTracker = ProgressTracker()

        @Suspendable
        @Throws(FlowException::class)
        override  fun call(): SignedTransaction {
            val tokenMXN : TokenType  = FiatCurrency.getInstance("MXN");
            val token : IssuedTokenType = tokenMXN issuedBy  ourIdentity;
            val cantidad = quantity of token;
          val fungibleToken = FungibleToken(cantidad,holder);
           return subFlow(IssueTokens(listOf(fungibleToken)))
        }

    }

    /**
     * Move created fungible tokens to other party
     */
    @StartableByRPC
    class MoveEvolvableFungibleTokenFlow(
                private val holder: Party,
                private val quantity: Long,
                val auditor : Party) : FlowLogic<SignedTransaction>() {

        override val progressTracker = ProgressTracker()

        @Suspendable
        @Throws(FlowException::class)
        override fun call(): SignedTransaction {
            val tokenMXN : TokenType  = FiatCurrency.getInstance("MXN");
            val amount = Amount(quantity,tokenMXN);
            val partyAndAmount = PartyAndAmount(holder,amount)
            if(holder.equals(auditor)) {
                return subFlow(MoveFungibleTokens(partyAndAmount = partyAndAmount))
            } else {
                return subFlow(MoveFungibleTokens(partyAndAmount = partyAndAmount, observers = listOf(auditor)))
            }
        }
    }

    /**
     * Holder Redeems fungible token issued by issuer
     */
    @StartableByRPC
    class RedeemHouseFungibleTokenFlow(
                                        private val tokenId: String,
                                       private val issuer: Party,
                                       private val quantity: Long,
                                        val monitor : Party) : FlowLogic<SignedTransaction>() {

        @Suspendable
        @Throws(FlowException::class)
        override fun call(): SignedTransaction {
            val notary = serviceHub.networkMapCache.notaryIdentities.first()
            val uuid = UUID.fromString(tokenId)
            val queryCriteria = QueryCriteria.LinearStateQueryCriteria(uuid  = listOf(uuid));
            val tokenStateAndRef = serviceHub.vaultService.queryBy<RealEstateEvolvableTokenType>(queryCriteria).states.single()
            val tokenPointer = tokenStateAndRef.state.data.toPointer<RealEstateEvolvableTokenType>()
            val amount = Amount(quantity,TokenType(tokenPointer.pointer.pointer.id.toString(),tokenPointer.fractionDigits))
            return subFlow(RedeemFungibleTokens(amount, issuer, listOf(monitor,notary)))
        }
    }


